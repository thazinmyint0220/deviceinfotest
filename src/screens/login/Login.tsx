import React, { useEffect, useState } from "react";
import {
  View,
  TextInput,
  Pressable,
  StatusBar,
  SafeAreaView,
  Modal,
  Platform,
  Text
} from "react-native";
import { HiraginoKakuText } from "../../components/StyledText";
import { Ionicons } from "@expo/vector-icons";
import styles from "./LoginStyles";
import { colors } from "../../styles/color";
import ModalPopup from "../completion/Completion";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { Button } from "../../components/basics/Button";
import { NavigationProp } from "@react-navigation/native";
import { realtimeDB } from "../../config/firebaseConfig";
import { get, ref } from "firebase/database";
import * as crypto from "crypto-js";
import * as Device from 'expo-device';
import Constants from 'expo-constants';
import * as Application from 'expo-application';
//import * as Notifications from 'expo-notifications';
import * as rdd from 'react-device-detect';
import {
  isSafari,
  isIOS,
  isMobile,
  isTablet,
  browserName,
  osName
} from 'react-device-detect';

type Props = {
  navigation: NavigationProp<any, any>;
};

export const Login = ({ navigation }: Props) => {
  const [userid, setUserId] = useState("");
  const [password, setPassword] = useState("");
  const [showPassword, setShowPassword] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");
  const [inputNotEmpty, setInputNotEmpty] = useState(false);
  const [isModalVisible, setModalVisible] = useState(false);
  const [isLogoutModalVisible, setLogoutModalVisible] = useState(false);
  const [expoPushToken, setExpoPushToken] = useState('');
  const [deviceInfoDetect, setDeviceInfoDetect] = useState<DeviceInfoType | null>(null);
  
  type DeviceInfoType = {
    userAgent: string;
    screenWidth: number;
    screenHeight: number;
    platform: string;
    //deviceMemory: string | number;
    browserName: string;
    osName: string;
    isSafari: boolean;
    isIOS: boolean;
    isMobile: boolean;
    isTablet: boolean;
  };

  useEffect(() => {
    const info: DeviceInfoType = {
      userAgent: navigator.userAgent,
      screenWidth: window.screen.width,
      screenHeight: window.screen.height,
      platform: navigator.platform,
      //deviceMemory: 'deviceMemory' in navigator ? navigator.deviceMemory??"" : 'N/A',
      browserName: browserName,
      osName: osName,
      isSafari: isSafari,
      isIOS: isIOS,
      isMobile: isMobile,
      isTablet: isTablet,
    };
    setDeviceInfo(info);
  }, []);

  //if (!deviceInfoDetect) return null;

  const checkLogin = () => {
    const userRef = ref(realtimeDB, "baseMember");
    // 'baseMember'ノードからデータをフェッチする
    get(userRef)
      .then((snapshot) => {
        if (snapshot.exists()) {
          const userData = snapshot.val();

          // ログインをチェック
          let loggedIn = false;
          for (const userId in userData) {
            if (userData.hasOwnProperty(userId)) {
              const user = userData[userId];
              let hashpassword = crypto
                .SHA256(password)
                .toString(crypto.enc.Hex);
              if (user.id === userid && user.password === hashpassword) {
                loggedIn = true;
                setErrorMessage("");
                navigation.navigate("EventList", { userId: "user03" });
                console.log("ログイン成功");
                break;
              }
            }
          }
          if (!loggedIn) {
            setErrorMessage("IDまたはパスワードが正しくありません");
            console.log("IDまたはパスワードが正しくありません");
          }
        } else {
          console.log("ユーザーデータが見つかりません");
        }
      })
      .catch((error) => {
        console.error("ユーザーデータの取得に失敗しました", error);
      });
  };

  const togglePasswordVisibility = () => {
    setShowPassword(!showPassword);
  };

  const handleInputChange = (text: string, isPassword: boolean = false) => {
    if (isPassword) {
      setPassword(text);
      setInputNotEmpty(userid.trim().length > 0 && text.trim().length > 0);
    } else {
      setUserId(text);
      setInputNotEmpty(text.trim().length > 0 && password.trim().length > 0);
    }
  };

  useEffect(() => {
    let timeOut = setTimeout(() => {
      closeModal();
    }, 10000);
    return () => clearTimeout(timeOut);
  });

  const [deviceInfo, setDeviceInfo] = useState({});
  //const [deviceModel, setDeviceModel] = useState('Unknown');

  useEffect(() => {
    setDeviceInfoDetect({      
      userAgent: navigator.userAgent,
      screenWidth: window.screen.width,
      screenHeight: window.screen.height,
      platform: navigator.platform,
     // deviceMemory: 'deviceMemory' in navigator ? navigator.deviceMemory : 'N/A',
      isSafari,
      isIOS,
      isMobile,
      isTablet,
      browserName,
      osName,
    });
  }, []);

  useEffect(() => {
    const userAgent = navigator.userAgent;
    //const model = getDeviceModel(userAgent);
    //setDeviceModel(model);
  }, []);

  // useEffect(() => {
  //   registerForPushNotificationsAsync()
  //     .then((token) => setExpoPushToken(token ?? ''))
  //     .catch((error: any) => setExpoPushToken(`${error}`));
  //   console.log("Device token : ", expoPushToken);
  // });

  const openCompletionModal = () => {
    setModalVisible(true);
  };

  const closeModal = () => {
    setModalVisible(false);
  };

  const openLogoutModal = () => {
    setLogoutModalVisible(true);
  };

  const handleFirstButton = () => {
    setLogoutModalVisible(false);
  };

  const handleSecondButton = () => {
    setLogoutModalVisible(false);
  };

  // const deviceId = Constants.deviceId;
  // console.log(" device id : ", deviceId);

  const brand = Device.brand;
  console.log("brand : ", brand);

  const deviceName = Device.deviceName;
  console.log("device name : ", deviceName);

  const deviceType = Device.deviceType;
  console.log("device type : ", deviceType);

  const manufacturer = Device.manufacturer;
  console.log("manufacturer : ", manufacturer);

  const modalId = Device.modelId;
  console.log("model Id : ", modalId);

  const modalName = Device.modelName;
  console.log("model name : ", modalName);

  const osBuildId = Device.osBuildId;
  console.log("Os Build Id : ", osBuildId);

  const osVersion = Device.osVersion;
  console.log("Os Version : ", osVersion);

  var idfv = null;

  const getIdfv = async () => {
    if (Platform.OS !== "web") {
      idfv = await Application.getIosIdForVendorAsync();
      console.log("idfv : ", idfv);
    }
  };

  console.log(getIdfv());
  
  const os = rdd.osVersion;
  console.log("osVersion : ", os," --- ",rdd.deviceType);
  console.log("---- ",rdd.isIPad13,'-----',rdd.OsTypes,'------',rdd.engineName,'---');

  function handleRegistrationError(errorMessage: string) {
    alert(errorMessage);
    throw new Error(errorMessage);
  }

  // async function registerForPushNotificationsAsync() {
  //   if (Platform.OS != "web") {
  //     if (Device.isDevice) {
  //       const { status: existingStatus } =
  //         await Notifications.getPermissionsAsync();
  //       let finalStatus = existingStatus;
  //       if (existingStatus !== 'granted') {
  //         const { status } = await Notifications.requestPermissionsAsync();
  //         finalStatus = status;
  //       }
  //       if (finalStatus !== 'granted') {
  //         handleRegistrationError('Permission not granted to get push token for push notification!');
  //         return;
  //       }
  //       const projectId =
  //         Constants?.expoConfig?.extra?.eas?.projectId ??
  //         Constants?.easConfig?.projectId;
  //       console.log('project id : ', projectId);
  //       if (!projectId) {
  //         handleRegistrationError('Project ID not found');
  //       }
  //       try {
  //         const pushTokenString = (
  //           await Notifications.getExpoPushTokenAsync({projectId : projectId})).data; //e.g. projectid >> "d9c64eab-9f08-4c0e-b053-593ccf0e60a7"
  //         console.log(pushTokenString);
  //         return pushTokenString;
  //       } catch (e: unknown) {
  //         handleRegistrationError(`${e}`);
  //       }
  //     } else {
  //       handleRegistrationError('Must use physical device for push notifications');
  //     }
  //   }
  // }
  // const getDeviceModel = (userAgent: any) => {
  //   // Example logic to determine the device model
  //   if (/iPad/.test(userAgent)) {
  //     if (/iPad11,6/.test(userAgent)) return 'iPad 9th Gen';
  //     if (/iPad12,1/.test(userAgent)) return 'iPad 10th Gen';
  //     // Add more specific user agent checks as needed
  //   }
  //   return 'Unknown iPad';
  // };

  return (
    <KeyboardAwareScrollView
      style={{ flex: 1, width: "100%" }}
      resetScrollToCoords={{ x: 0, y: 0 }}
      contentContainerStyle={styles.mainContainer}
      scrollEnabled={false}
    >
      <SafeAreaView style={styles.mainContainer}>
        <StatusBar barStyle="dark-content" />
        <View style={styles.headerContainer}>
          <HiraginoKakuText style={styles.headerText}>
            受付システム
          </HiraginoKakuText>
        </View>
        <View style={styles.bodyContainer}>
          <HiraginoKakuText style={styles.loginText}>ログイン</HiraginoKakuText>
          <View style={styles.infoBox}>
            <View style={styles.inputContainer}>
              <View style={styles.labelInputSetBox}>
                <HiraginoKakuText style={styles.label}>ID</HiraginoKakuText>
                <TextInput
                  style={styles.input}
                  placeholder="ID"
                  placeholderTextColor={colors.placeholderTextColor}
                  onChangeText={(text) => handleInputChange(text, false)}
                  value={userid}
                />
              </View>

              <View style={styles.labelInputSetBox}>
                <HiraginoKakuText style={styles.label}>
                  パスワード
                </HiraginoKakuText>
                <View>
                  <TextInput
                    secureTextEntry={!showPassword}
                    placeholder="パスワード"
                    placeholderTextColor={colors.placeholderTextColor}
                    value={password}
                    onChangeText={(text) => handleInputChange(text, true)}
                    style={styles.input}
                  />
                  <Pressable
                    style={styles.eyeIconContainer}
                    onPress={togglePasswordVisibility}
                    hitSlop={16}
                  >
                    {showPassword ? (
                      <Ionicons
                        name="eye"
                        size={24}
                        color="black"
                        style={styles.eyeIcon}
                      />
                    ) : (
                      <Ionicons
                        name="eye-off"
                        size={24}
                        color="black"
                        style={styles.eyeIcon}
                      />
                    )}
                  </Pressable>
                  <View style={styles.messageContainer}>
                    {errorMessage !== "" && (
                      <HiraginoKakuText style={styles.errorMessage} normal>
                        {errorMessage}
                      </HiraginoKakuText>
                    )}
                  </View>
                </View>
              </View>
            </View>
            <Button
              text="ログイン"
              onPress={checkLogin}
              style={styles.buttonLogin}
              type={inputNotEmpty ? "ButtonMPrimary" : "ButtonMDisable"}
            />
            <View>
            <Text>1. User Agent: {deviceInfoDetect?.userAgent}{'　'}2. Screen Width: {deviceInfoDetect?.screenWidth}
              {'　'}3.Screen Height: {deviceInfoDetect?.screenHeight}
              {'　'}4.Platform: {deviceInfoDetect?.platform}
              {'　'}5.Browser: {deviceInfoDetect?.browserName}{'　'}6.OS: {deviceInfoDetect?.osName}
              {'　'}7.Is Safari: {deviceInfoDetect?.isSafari ? 'Yes' : 'No'}
              {'　'}8.Is iOS: {deviceInfoDetect?.isIOS ? 'Yes' : 'No'}
              {'　'}9.Is Tablet: {deviceInfoDetect?.isTablet ? 'Yes' : 'No'}</Text>
            </View>
          </View>
        </View>
        <Modal
          transparent={true}
          visible={isModalVisible}
          onRequestClose={closeModal}
        >
          <ModalPopup closeModal={closeModal} />
        </Modal>
      </SafeAreaView>
    </KeyboardAwareScrollView>
  );
};
